#ifndef LEVENSHTEIN_H
#define LEVENSHTEIN_H

int levenshtein(char *s1, char *s2, int workers);
int** create_matrix(int rows, int cols);
void free_matrix(int** matrix, int rows);
int parallel_levdist(char* text, char* pattern, int t_len, int p_len, int ideal_recommended);
int get_c(char searching, char* alphabet);
int MIN3(int a, int b, int c);

#endif // LEVENSHTEIN_H
