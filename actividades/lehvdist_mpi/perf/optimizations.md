# Optimizations made for Lehvdist #

[TOC]

## Profiling ##

The process of profiling is, to check which instructions are called or take more time to execute in a program.

Or, as explained by wikipedia:
*In software engineering, profiling is a form of dynamic program analysis that measures, for example, the space (memory) or time complexity of a program, the usage of particular instructions, or the frequency and duration of function calls. Most commonly, profiling information serves to aid program optimization.* [(source)](https://en.wikipedia.org/wiki/Profiling_(computer_programming).

A subroutine can be optimized in terms of CPU usage, speed, and cache uses. Our program's performance was measured in these terms in the following sections.

#### Callgrind ####

We have to analyse wich subroutine eats up more CPU usage.
The callgrind profiling tool and the kcachegrind visualizer where tools used for this analysis.
After the profiling, we noticed how much of the procesing was taking place in the lehvenshtein algorithm. This method was called levenshtein(). It measured the lehvenshtein distance between two given strings.

![levdist](lev1.png)

In the map, it can be observed how the levenshtein algorithm actually makes up almost 95% of the calls:

![map](lev2.png)

For this reason we will attempt to optimize the lehvenshtein algorithm.

#### Gprof ####

gprof is a tool we used to measure which method the program spent more time in.
After opening and procesing this program with gprof, there was no doubt that the subroutine causing problem was the main lehvenshtein() function. After this function, another subroutine that was called a lot of times was structGreaterThan(), used for sorting the comparations. The full document can be seen at gprof.txt, in the same directory this .md is in.

gprof was called this way:

```
 1268  make clean
 1269  make 'FLAGS= -Wall -Wextra -pg'
 1270  bin/levdist -q -r ../../../../pruebas_profe/
 1271  ls -alrt
 1272  gprof ./bin/levdist gmon.out > perf/gprof.txt
```

Here is the most important section of the document:

```
Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls  Ks/call  Ks/call  name    
100.05   3196.02  3196.02   241165     0.00     0.00  levenshtein
  0.00   3196.07     0.05        1     0.00     3.20  levdist_compare_files
  0.00   3196.10     0.03  1961596     0.00     0.00  structEquals
  0.00   3196.12     0.02                             struct_compare_func
  0.00   3196.13     0.01  3951686     0.00     0.00  structGreaterThan
```
Notice how levenshtein() now stands out more than in the callgrind analysis, showing a 100.05% of running time.

#### Cachegrind ####

We can determine which function uses the most of cache memory and its misses with cachegrind. We used the kcachegrind visualizer again.

##### What is a cache miss? #####
*Cache miss is a state where the data requested for processing by a component or application is not found in the cache memory. It causes execution delays by requiring the program or application to fetch the data from other cache levels or the main memory.* [(source)](https://www.techopedia.com/definition/6308/cache-miss).

![cache: visualization of kcachegrind](lev3.png)

With kcachegrind we could observe how levenshtein() is again the method that causes more problems. This time cache-wise (93.48%!).

Our cache misses are happening inside of levenshtein(). It occurs because the levenshtein information (stored in a matrix) is constantly updating itself, causing cache misses because the data is changing. Calculations depend on previous sections (of the matrix) being computed. This updates the matrix and forces cache misses for the next processes.

## Optimizing lehvenshtein() ##

We found a solution for the algorithm that used only the previous row instead of the previous row and column. This is done by doing complex calculations and an additional matrix. More info can be found [here](https://ieeexplore.ieee.org/document/6665373).

To ensure this version was still viable, we performed hand tests of the previous algorithm and the new algorithm we were about to test. Both test showed succesful, as shown in the following images:


![levdist](handtest1.png)
![levdist](handtest2.png)