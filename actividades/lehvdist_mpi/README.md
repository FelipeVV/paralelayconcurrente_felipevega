# Levenshtein Distance

Finds most similar files stored in directories using the Levenshtein Distance algorithm. The program finds all the files to compare, compares all of them with each other and produces an output in order from less to more distance. The comparisons of files that are more similar will be shown first.

For more theory on Levenshtein distance read: The String-to-String Correction Problem at https://dl.acm.org/citation.cfm?doid=321796.321811.

More informarion at http://www.levenshtein.net/.

A distance of 1 can be measured by either subtituting, deleting or adding a char. "hello" and "hello" have distance of 0. "hello" and "hello!" have a distance of one, since an adition was required at the end.
"hello" and "helPlo" have a distance of one too, as an insertion was needed.

This program can be used, for example, to compare homework to check if students are copying each other.
This program may check if the Levenshtein distance between the assignments is short and the students had identical phrases/sections in their assignments.

Levenshtein distance is also useful for spell checking. In linguistics, the algorithm may be useful for figuring how distant are some languages from each other. An example of this is the word for "idea" in different laguages:

* "idea"(english) and "idea"(spanish). Distance is 0.
* "idea"(english) and "idee"(german). Distance is 1.
* "idea"(english) and "ideya"(bulgarian). Distance is 1.
* "idea"(english) and "fikra"(arabic). Distance is 3.

From this it can be considered that spanish, english, german and bulgarian may have the same origin for
 the word for "idea"(greek "idein"), while arabic has a different origin.


## User manual

Once installed, the program may be called by typing 'levdist' followed by the folders one wishes to compare.
The program has the following flags that may be used for the following:

### --help ###

Prints help that specifies commands.

### --version ###

Prints information about this command.  

### -q, --quiet ###

Do not print elapsed time.

### -Q, --silent ###
Do not generate output at all.

### -r, --recursive ###

Analyze files in subdirectories.

Recursive option is useful when checking a lot of files. 

## Building ##

Requisites:

1. A POSIX Unix operating system
2. A GCC-compatible compiler


Once downloaded, this program may be installed by typing 'make install' in the command prompt, while inside the directory containing the Makefile.
After this, the program may be called by typing 'levdist' followed by any arguments the user may use.
Arguments like the folders one wishes to compare and flags.

Additionaly, the levdist program can be run in debug mode by writing 'make'. 
To remove any build files write 'make clean'. This is recommended when changing builds. For example when changing from debug mode to release.
To build in release mode write 'make release'.

## Testing ##

To test the validity of the program, run 'make test' in the terminal. 
Testing is done through a *black box* procedure. 

## Authors: ##

Felipe Vega. felipevegavindas1998@gmail.com.

This work was part of an assignment for a course at University of Costa Rica.

Software may be used under the MIT License (specified in ./License.txt).

Levenshtein code taken from wikibooks: https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance. Under the Creative Commons Attribution-ShareAlike License.

Tree is a modified version of Gist's bst. https://gist.github.com/ArnonEilat/4611213. 