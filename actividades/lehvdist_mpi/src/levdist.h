#ifndef LEVDIST_H
#define LEVDIST_H


/** @file levdist.h

This file contains the structures and functions that control the entire
`levdist` command. It can be thought as the global controller object
in object-oriented programming.

It may be created in the main function, for example:

```
int main(int argc, char* argv[])
{
	levdist_t levdist;
	levdist_init(&levdist);
	return levdist_run(&levdist, argc, argv);
}
```
*/

#include "arguments.h"
#include "queue.h"


typedef struct
{
    int dist;
    char* path1;
    char* path2;

} comp_info;

/**
	@brief Stores the atributes shared for the controller functions in this file.
*/
typedef struct
{
	/// What user asked by the command-line arguments.
	arguments_t arguments;
	/// Queue of files to be compared using Levenshtein distance.
	queue_t* files;
    /// Struct containing pair results haha~
    comp_info* struct_array;
    /// contains how many struct arrays we have inserted;
    int struct_array_count;
    /// My rank
    int my_rank;
    /// Total processes
    int process_count;
} levdist_t;


bool structGreaterThan(const comp_info *a, const comp_info *b);
bool structEquals(const comp_info *a, const comp_info *b);
int struct_compare_func(const void *a, const void *b);

/**
	@brief Initializes a record with default information required by the controller.

	@param this Pointer to the @a levdist_t structure to be initialized.
*/
void levdist_init(levdist_t* this);


void print_struct_array(levdist_t* this);

/**
	@brief Start the execution of the command

	@param this Pointer to the @a levdist_t structure with the shared attibutes.
	@param argc Argument count provided from the `main()` function.
	@param argv Argument vector provided from the `main()` function.
	@return The exit status, where 0 stands for success, otherwise for an error.
*/
int levdist_run(levdist_t* this, int argc, char* argv[]);


/**
	@brief Finds all files in the directories given by arguments, and load them to
	the @a this->files queue.

	@param this Pointer to the @a levdist_t structure with the shared attibutes.
	@param argc Argument count provided from the `main()` function.
	@param argv Argument vector provided from the `main()` function.
	@return The exit status, where 0 stands for success, otherwise for an error.
*/
int levdist_process_dirs(levdist_t* this, int argc, char* argv[]);


/**
	@brief Traverses all the directories provided by user in command-line arguments, and
	load all files to the @a this->files queue.

	@param this Pointer to the @a levdist_t structure with the shared attibutes.
	@param argc Argument count provided from the `main()` function.
	@param argv Argument vector provided from the `main()` function.
	@return The exit status, where 0 stands for success, otherwise for an error.
*/
int levdist_list_files_in_args(levdist_t* this, int argc, char* argv[]);

bool levdist_load_files_array(levdist_t* this);

int calculate_distance(comp_info* struct_array, size_t* distances_array, size_t workers, size_t start, size_t finish);

#endif // LEVDIST_H
