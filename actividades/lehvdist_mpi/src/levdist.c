#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#include <mpi.h>

#include "concurrency.h"
#include "dir.h"
#include "levdist.h"
#include "levenshtein.h"


#define MIN(a,b) (((a)<=(b))?(a):(b))

// Private functions:

/// Shows how to travese the list removing elements
void levdist_print_and_destroy_files(levdist_t* this);

/// Shows how to traverse the list without removing elements
void levdist_print_files(levdist_t* this);


void levdist_init(levdist_t* this)
{
	arguments_init(&this->arguments);
	this->files = NULL;
}

int levdist_run(levdist_t* this, int argc, char* argv[])
{
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &this->process_count);
    MPI_Comm_rank(MPI_COMM_WORLD, &this->my_rank);
    MPI_Barrier(MPI_COMM_WORLD);
    // Analyze the arguments given by the user
	this->arguments = arguments_analyze(argc, argv);

	// If arguments are incorrect, stop
	if ( this->arguments.error )
		return this->arguments.error;

	// If user asked for help or software version, print it and stop
	if ( this->arguments.help_asked )
		return arguments_print_usage();
	if ( this->arguments.version_asked )
		return arguments_print_version();

	// If user did not provided directories, stop
	if ( this->arguments.dir_count <= 0 )
    {
        return (fprintf(stdout, "levdist: error: no directories given\n")), 1;
    }

    // Arguments seems fine, process the directories
    size_t dist = levdist_process_dirs(this, argc, argv);
    return dist;
}

bool levdist_load_files_array(levdist_t* this)
{
    bool aComparationWasMade = false;
    for( queue_iterator_t itr1 = queue_begin(this->files); itr1 != queue_end(this->files); itr1 = queue_next(itr1) )
    {
        for( queue_iterator_t itr2 = queue_next(itr1); itr2 != queue_end(this->files); itr2 = queue_next(itr2) )
        {
            char* filename1 = (char*)queue_data(itr1);
            char* filename2 = (char*)queue_data(itr2);
            if( strcmp(filename1, filename2) != 0 )
            {
                aComparationWasMade = true;


                //Create item for inserrting in our struct array
                comp_info itm;
                itm.dist = 0;
                if(strcmp(filename1, filename2) < 0)
                {
                    itm.path1 = filename1;
                    itm.path2 = filename2;
                }
                else
                {
                    itm.path1 = filename2;
                    itm.path2 = filename1;
                }

                this->struct_array[this->struct_array_count] = itm;
                ++this->struct_array_count;
                ///printf("Compare:[%s][%s]\n", filename1, filename2);
            }
        }
    }
    return aComparationWasMade;
}

int calculate_distance(comp_info* struct_array, size_t* distances_array, size_t workers, size_t start, size_t finish)
{
    for(size_t index = start; index < finish; ++index)
    {
        // read file 1
        FILE *pFile; //struct muy saico que tiene informacion del file
        char * buffer;
        struct stat file_info;
        // open files
        pFile = fopen( struct_array[index].path1, "rb" );
        if(pFile == NULL)
        {
            fprintf(stderr, "File error");
            exit(1);
        }

        stat( struct_array[index].path1, &file_info );

        // allocate memory to contain the whole file
        buffer = (char *) malloc(sizeof(char)*file_info.st_size+1);
        if(buffer==NULL)
        {
            fprintf(stderr, "Memory error.");
            exit(2);
        }
        size_t result = fread(buffer, 1, file_info.st_size, pFile);
        buffer[file_info.st_size] = '\0';
        if((long)result != file_info.st_size)
        {
            fprintf(stderr, "Reading error.");
            exit(3);
        }

        // read file 2
        FILE *pFile2; //struct muy saico que tiene informacion del file
        char * buffer2;
        struct stat file_info2;
        // open files
        pFile2 = fopen( struct_array[index].path2, "rb" );
        if(pFile2 == NULL)
        {
            fprintf(stderr, "File error");
            exit(1);
        }

        stat( struct_array[index].path2, &file_info2 );

        // allocate memory to contain the whole file
        buffer2 = (char *) malloc(sizeof(char)*file_info2.st_size+1);
        if(buffer2==NULL)
        {
            fprintf(stderr, "Memory error.");
            exit(2);
        }
        size_t result2 = fread(buffer2, 1, file_info2.st_size, pFile2);
        buffer2[file_info2.st_size] = '\0';
        if((long)result2 != file_info2.st_size)
        {
            fprintf(stderr, "Reading error.");
            exit(3);
        }

        distances_array[index] = levenshtein(buffer, buffer2, workers);

        free(buffer);
        free(buffer2);
        fclose(pFile);
        fclose(pFile2);

    }
    return 0;
}

bool structGreaterThan(const comp_info *a, const comp_info *b)
{
    if(a->dist > b->dist)
        return true;
    else if(a->dist == b->dist)
    {
        // If numbers are equal they must be compared by string
        if( strcmp(a->path1, b->path1) > 0 )
            return true;
        else if( strcmp(a->path1, b->path1) == 0)
        {
            // J.F.C dist and path1 are equal, compare path2
            if( strcmp(a->path2, b->path2) > 0 )
                return true;
            else if( strcmp(a->path2, b->path2) == 0 )
            {
                // Everything is equal
                return false; //!
            }
        }
    }
    return false;
}

bool structEquals(const comp_info *a, const comp_info *b)
{
    if( (a->dist == b->dist)&&(strcmp(a->path1, b->path1) == 0)&&(strcmp(a->path2, b->path2) == 0) )
        return true;
    return false;
}

int struct_compare_func(const void *a, const void *b)
{
    const comp_info *struct1 = (const comp_info*)a;
    const comp_info *struct2 = (const comp_info*)b;
    if(structGreaterThan(struct1, struct2))
        return 1;
    else if(structEquals(struct1, struct2))
        return 0;
    return -1;
}

int levdist_process_dirs(levdist_t* this, int argc, char* argv[])
{
	// Load all files into a list
	this->files = queue_create();

    // Fill queue
    levdist_list_files_in_args(this, argc, argv);

    // Ver si solo se esta comparando un archivo
    bool aComparationWasHypotheticallyMade = false;
    size_t queue_size = 0;
    for( queue_iterator_t itr1 = queue_begin(this->files); itr1 != queue_end(this->files); itr1 = queue_next(itr1) )
    {
        ++queue_size;
        for( queue_iterator_t itr2 = queue_next(itr1); itr2 != queue_end(this->files); itr2 = queue_next(itr2) )
        {
            char* filename1 = (char*)queue_data(itr1);
            char* filename2 = (char*)queue_data(itr2);
            if( strcmp(filename1, filename2) != 0 )
            {
                aComparationWasHypotheticallyMade = true;
            }
        }
    }
    if(!aComparationWasHypotheticallyMade)
    {
        return (void)(fprintf(stderr, "levdist: error: at least two files are required to compare\n")), 1;
    }

    // Create struct array of size Gauss(n-1)
    size_t array_size = ((queue_size-1)*queue_size)/2;
    this->struct_array = malloc( sizeof(comp_info) * array_size );

    // Make sure memory was allocated
    if(this->struct_array == NULL) return (void)(fprintf(stderr, "bang bang\n")), 2;

    this->struct_array_count = 0;

    // Load files into the array
    levdist_load_files_array(this);

    size_t* distances_array = calloc(this->struct_array_count, sizeof(size_t));
    size_t c = this->struct_array_count / this->process_count;
    size_t r = this->struct_array_count % this->process_count;
    size_t start = this->my_rank * c + MIN((long long)this->my_rank, (long long)r);
    size_t finish = (this->my_rank+1) * c + MIN((long long)(this->my_rank+1), (long long)r);

    double begin = MPI_Wtime();
    calculate_distance(this->struct_array, distances_array, this->arguments.workers, start, finish);

    double end = MPI_Wtime();
    double duration = end - begin;
    double max_duration = -1.0;
    MPI_Reduce(&duration, &max_duration, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);

    if(this->my_rank !=0)
    {
        MPI_Send(distances_array, this->struct_array_count, MPI_UNSIGNED_LONG_LONG, 0, 0, MPI_COMM_WORLD);
        MPI_Send(&start, 1, MPI_UNSIGNED_LONG_LONG, 0, 1, MPI_COMM_WORLD);
        MPI_Send(&finish, 1, MPI_UNSIGNED_LONG_LONG, 0, 2, MPI_COMM_WORLD);
    }

    if(this->my_rank == 0)
    {
        for(size_t index = start; index < finish; ++index)
            this->struct_array[index].dist = distances_array[index];
        for(int recieve = 1; recieve < this->process_count; ++recieve)
        {
            MPI_Recv(distances_array, this->struct_array_count, MPI_UNSIGNED_LONG_LONG, recieve, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            MPI_Recv(&start, 1, MPI_UNSIGNED_LONG_LONG, recieve, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            MPI_Recv(&finish, 1, MPI_UNSIGNED_LONG_LONG, recieve, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            for(size_t index = start; index < finish; ++index)
                this->struct_array[index].dist = distances_array[index];

        }
        // Sort array
        qsort(this->struct_array, this->struct_array_count, sizeof(comp_info), struct_compare_func);

        // print struct array you mf
        if(!this->arguments.silent_asked)
            print_struct_array(this);

        // Report elapsed time
        if(!this->arguments.quiet_asked)
        {
           printf("Duration %.9lf seconds\n", max_duration);
        }

    }

//	// Report elapsed time
//    if(!this->arguments.quiet_asked)
//    {
//        printf("Total time %.9lfs. Comparing time %.9lfs. Workers: %d.\n",
//               walltime_elapsed(&total_start),
//               elapsed_comparison,
//               this->arguments.workers);
//    }

    // Destroy the element of the queue
    queue_destroy(this->files, true);

    // Free struct
    free(this->struct_array);

    // Free distances array
    free(distances_array);
    MPI_Finalize();
	return 0;
}

int levdist_list_files_in_args(levdist_t* this, int argc, char* argv[])
{
	// Traverse all arguments
	for ( int current = 1; current < argc; ++current )
	{
		// Skip command-line options
		const char* arg = argv[current];
		if ( *arg == '-' )
			continue;

		dir_list_files_in_dir(this->files, arg, this->arguments.recursive);
	}

	return 0;
}

void levdist_print_and_destroy_files(levdist_t* this)
{
	long count = 0;
	while ( ! queue_is_empty(this->files) )
	{
		char* filename = (char*)queue_pop(this->files);
        printf("%ld: %s\n", ++count, filename);
		free(filename);
	}
}

void levdist_print_files(levdist_t* this)
{
	long count = 0;
	for ( queue_iterator_t itr = queue_begin(this->files); itr != queue_end(this->files); itr = queue_next(itr) )
    {
		const char* filename = (const char*)queue_data(itr);
        printf("%ld: %s\n", ++count, filename);
	}
}

void print_struct_array(levdist_t *this)
{
    for(int index = 0; index < this->struct_array_count; ++index)
    {
        fprintf(stdout,"%d\t%s\t%s\n", this->struct_array[index].dist, this->struct_array[index].path1, this->struct_array[index].path2);
    }
}
