## Diseñando el esquema de distribución

Los datos se van a dividir entre las máquinas usando el esquema estático por bloques.
Con este esquema es fácil de implementar sobre la solución de concurrencia implementada(OMP)
y requiere menos mantenimiento. 

## Especificaciones de los procesos

Cada proceso tendrá su propia copia de la estructura de datos con las parejas a comparar.
También manejarán los argumentos y y la lista de archivos.
Cada proceso calcula su rango en la estructura a calcular y saca las distancias.

## Pseudocódigo


Initialize MPI
Conseguir el numero de proceso, my_rank
Conseguir la cantidad de máquinas, process_count
De los argumentos, conseguir la lista de archivos a comparar
Crear un arreglo de distancias levenshtein basado en las parejas listadas
Calcular el rango de parejas que me toca revisar
Calcular las distancias de levenshtein que me tocan
Enviar las distancias al proceso 0
If (soy el proceso 0)
* recibir el arreglo de cada proceso
* poner distancias en el arreglo de estructuras principal
* sort arreglo
* imprimir la salida al usuario

Finalize MPI
