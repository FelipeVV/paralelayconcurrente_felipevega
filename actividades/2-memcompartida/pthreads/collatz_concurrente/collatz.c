#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define MIN(a,b) (((a)<(b))?(a):(b))

typedef struct Info
{ 
   size_t given_id;
} t_worker_info; 

void* calculate(void* data);

static size_t converged_ints = 0; // Used to know how many threads have actualy finished measuring
static size_t measured_step = 0;
static size_t worker_count = 3;
static size_t number_count = 3;
static size_t* numbers = NULL;
static size_t current_step = 0;
static size_t max_steps = 10;
static pthread_t* workers = NULL; //Array for storing our workers
static pthread_barrier_t barrier;

int main()
{
	printf("amount of numbers/workers: ");
	scanf("%lu", &number_count);
	worker_count = number_count;
	numbers = malloc( number_count * sizeof(size_t) );
	for ( size_t index = 0; index < number_count; ++index )
	{
		printf("Number #%zu: ", index+1);
		scanf("%zu", &numbers[index]);
	}
	
	pthread_barrier_init(&barrier, NULL, (unsigned)worker_count);
	t_worker_info worker_data_array[worker_count];

	workers = malloc(worker_count * sizeof(pthread_t));
	for ( size_t index = 0; index < worker_count; ++index )
	{
		worker_data_array[index].given_id = index;
		pthread_create(&workers[index], NULL, calculate, (void*)&worker_data_array[index]);
	}
	for ( size_t index = 0; index < worker_count; ++index )
		pthread_join(workers[index], NULL);

	pthread_barrier_destroy(&barrier);

	if ( measured_step < max_steps )
		printf("Converged in %zu steps\n", measured_step+1);
	else
		printf("No converge in %zu steps\n", max_steps);

	return 0;
}

void* calculate(void* data)
{
	t_worker_info* my_info = (t_worker_info*)data;
	const size_t my_id = my_info->given_id;
	const size_t next = (my_id + 1) % worker_count;
	size_t modulo_fix = my_id + worker_count;
	const size_t prev = (modulo_fix - 1) % worker_count;
//	fprintf(stderr, "next of %zu is %zu\n", my_id, next);
//	fprintf(stderr, "prev of %zu is %zu\n", my_id, prev);

	/// Esta variable se usa para aviarle a todos los otros threads de que ya termino, 
	/// pero solo lo puede avisar una vez.
	bool canEnterCompleteCondition = true;
	while ( current_step < max_steps )
	{
		/// Enumerator that tells the number how to modify itself.
		enum following_action{nothing, even, odd};
		enum following_action action = nothing;
		size_t change_value = 0;
		if( numbers[my_id] != 1 )
		{
			if ( numbers[my_id] % 2 == 0 )
				action = even;
			else
			{
				change_value = numbers[prev] * (numbers[my_id] + numbers[next]); //guardar cambio que hacer
//				fprintf(stderr, "%zu: %zu * (%zu + %zu)\n",my_id, numbers[prev], numbers[my_id], numbers[next]);
				action = odd;
			}
		}
		
		
		pthread_barrier_wait(&barrier);
		/// Actualizar los cambios
		if(action != nothing)
		{
			if(action == even)
			{
//				fprintf(stderr, "%zu: div by 2\n", numbers[my_id]);
				numbers[my_id] /= 2;
			}
			else //odd
			{
//				fprintf(stderr, "%zu: do odd thing\n", numbers[my_id]);
				numbers[my_id] = change_value;
			}
		}
		/*else
		{
			fprintf(stderr, "%zu: do nothing\n", numbers[my_id]);
		}*/

		pthread_barrier_wait(&barrier);

		/// Si ya es 1, avisar
		if((numbers[my_id] == 1) && canEnterCompleteCondition)
		{
			canEnterCompleteCondition = false;
			converged_ints++;
		}
		
		if ( my_id == 0 )
		{
			fprintf(stderr, "[%zu][%zu][%zu]\n", numbers[my_id], numbers[my_id+1], numbers[my_id+2]);
			++current_step;
			if( converged_ints < worker_count )
				measured_step++;
		}
		pthread_barrier_wait(&barrier);
	}
	
	return NULL;
}
