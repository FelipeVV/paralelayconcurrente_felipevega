## Rastree la memoria y el procesamiento del programa.

Hecho en un cuaderno.

## Asuma tres valores en la entrada estándar.

Hecho. https://imgur.com/tUC8vFx.

## ¿Resuelve el código anterior el problema planteado?

No, la barrier esta mal implementada. Si un thread termina su proceso fuera del while, no se vuelve a registrar en la barrier y los otros threads se quedan prensados.
Tambien, los cambios se hacen de inmediato. Para corregirlo, implementé una forma de que basado en sus vecinos, el thread apunta como es que su numero debería cambiar. Luego entre dos barriers, hace el cambio.