#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>


void* hello(void* data)
{
	size_t thread_number = (size_t)data;
	printf("Hello from thread %zu\n", thread_number);
	return NULL;
}

int main(int argc, char* argv[])
{
	int ideal_number_of_workers = 0;
	// If number of workers specified
	if( (argc > 1) && (atoi(argv[1]) > 0 ) )
		ideal_number_of_workers = atoi(argv[1]); // set ideal_ to that value
	else
		ideal_number_of_workers = sysconf(_SC_NPROCESSORS_ONLN);

	
	// Create worker array
	pthread_t workers[ideal_number_of_workers];
	// Put workers to work
	for(size_t index = 0; index < ideal_number_of_workers; ++index)
	{
		pthread_create( &workers[index], NULL, hello, (void*)index );
	}
	// Print hello
	printf("Hello from main thread\n");
	// Wait for all workers to finish before ending program
	for(size_t index = 0; index < ideal_number_of_workers; ++index)
	{
		pthread_join(workers[index], NULL);
	}
	return 0;
}


