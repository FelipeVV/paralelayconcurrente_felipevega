#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

typedef struct{
   size_t my_id;
   size_t total_workers;
} worker_data_t;

void* hello(void* data)
{
	worker_data_t *my_data = (worker_data_t*)data;
	printf("Hello from thread %zu of %zu!\n", my_data->my_id, my_data->total_workers);
	return NULL;
}

int main(int argc, char* argv[])
{
	int ideal_number_of_workers = 0;
	// If number of workers specified
	if( (argc > 1) && (atoi(argv[1]) > 0 ) )
		ideal_number_of_workers = atoi(argv[1]); // set ideal_ to that value
	else
		ideal_number_of_workers = sysconf(_SC_NPROCESSORS_ONLN);

	// Create worker array
	pthread_t workers[ideal_number_of_workers];
	// Create data array
	worker_data_t worker_data_array[ideal_number_of_workers];
	
	// Put workers to work
	for(size_t index = 0; index < ideal_number_of_workers; ++index)
	{
		worker_data_array[index].my_id = index;
		worker_data_array[index].total_workers = ideal_number_of_workers-1;
		pthread_create( &workers[index], NULL, hello, (void*)&worker_data_array[index] );
	}
	// Print hello
	printf("Hello from main thread\n");
	// Wait for all workers to finish before ending program
	for(size_t index = 0; index < ideal_number_of_workers; ++index)
	{
		pthread_join(workers[index], NULL);
	}
	return 0;
}


