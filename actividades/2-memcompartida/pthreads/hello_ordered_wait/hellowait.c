#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include "concurrency.h"

typedef struct{
   size_t my_id;
   size_t total_workers;
   size_t *number_to_print;
} worker_data_t;

void* hello(void* data)
{
	worker_data_t *my_data = (worker_data_t*)data;
	while ( *(my_data->number_to_print) < my_data->my_id )
		; //busy waiting
	printf("Hello from thread %zu of %zu!\n", my_data->my_id, my_data->total_workers);
	++(*(my_data->number_to_print));
	return NULL;
}

int main(int argc, char* argv[])
{
	// Start counting the time
	walltime_t start;
	walltime_start(&start);

	int ideal_number_of_workers = 0;
	// If number of workers specified
	if( (argc > 1) && (atoi(argv[1]) > 0 ) )
		ideal_number_of_workers = atoi(argv[1]); // set ideal_ to that value
	else
		ideal_number_of_workers = sysconf(_SC_NPROCESSORS_ONLN);

	// Create worker array
	pthread_t workers[ideal_number_of_workers];
	// Create data array
	worker_data_t worker_data_array[ideal_number_of_workers];
	size_t main_number_to_print = 0;
	// Put workers to work
	for(size_t index = 0; index < ideal_number_of_workers; ++index)
	{
		worker_data_array[index].my_id = index;
		worker_data_array[index].total_workers = ideal_number_of_workers-1;
		worker_data_array[index].number_to_print = &main_number_to_print;
		pthread_create( &workers[index], NULL, hello, (void*)&worker_data_array[index] );
	}
	// Print hello
	printf("Hello from main thread\n");
	// Wait for all workers to finish before ending program
	for(size_t index = 0; index < ideal_number_of_workers; ++index)
	{
		pthread_join(workers[index], NULL);
	}

	fprintf(stdout, "Elapsed %.3lfs.\n", walltime_elapsed(&start));

	return 0;
}


