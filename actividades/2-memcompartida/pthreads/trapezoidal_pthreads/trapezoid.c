#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <semaphore.h>

#include "concurrency.h"

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

typedef struct{
   size_t my_id;
   size_t my_total_workers;
   sem_t *shared_semaphores;
   double my_delta;
   double my_sum;
   double my_a;
   double my_n;
} worker_data_t;

/// cool function
double f(double x)
{
	return x + sin(2.0*x);
}

/// https://wikimedia.org/api/rest_v1/media/math/render/svg/bfac656c5d75fb6dd578fd1a96cc5d23ef6a3692
void* trapezoidal_rule(void* data)
{
	worker_data_t *my_data = (worker_data_t*)data;

	size_t dist = my_data->my_n / my_data->my_total_workers;
	size_t start = my_data->my_id * dist;
	size_t end = (my_data->my_id+1) * dist;
//	fprintf(stderr, "Checking [%lu - %lu[\n", start, end);
	for(size_t i = start; i < end; ++i)
		my_data->my_sum += 2.0*f(my_data->my_a + i * my_data->my_delta);
	return NULL;
}



int main(int argc, char* argv[])
{
	// Start of the range
	double a = 0.0;
	// End of the range
	double b = 0.0;
	// Amount of trapezoids to use
	double n = 1.0;
	// Amount of workers to split the trapezoids into
	size_t ideal = sysconf(_SC_NPROCESSORS_ONLN);

	if( argc > 3 )
	{
		a = (double)atof(argv[1]);
		b = (double)atof(argv[2]);
		n = (double)atof(argv[3]);
		if(argc > 4)
			ideal = atoi(argv[4]);
	}
	else
		return fprintf(stderr, "Invalid input!\n"), 1;

	// fix ideal
	ideal = MIN(ideal, n);

	// Start counting the time
	walltime_t start;
	walltime_start(&start);
	
	// Create worker array
	pthread_t workers[ideal];
	// Create data array
	worker_data_t worker_data_array[ideal];
	// Create semaphore array
	sem_t semaphores[ideal];

	// Initialize sems
	sem_init(&semaphores[0], 0, 1);
  	for(size_t index = 1; index < ideal; ++index)
	{
	   sem_init(&semaphores[index], 0, 0);
	}

	double sum = 0.0;
	double delta = (b-a)/n;
	
	// Independly sum the first and last elements
	sum += f(a + 0.0*delta) + f(a + n*delta);
	

	// Put workers to work
	for(size_t index = 0; index < ideal; ++index)
	{
		worker_data_array[index].my_id = index;
		worker_data_array[index].my_total_workers = ideal;
		worker_data_array[index].shared_semaphores = semaphores;
		worker_data_array[index].my_delta = delta;
		worker_data_array[index].my_sum = 0.0;
		worker_data_array[index].my_a = a;
		worker_data_array[index].my_n = n;
		pthread_create( &workers[index], NULL, trapezoidal_rule, (void*)&worker_data_array[index] );
	}

	// Wait for all workers to finish before ending program
	for(size_t index = 0; index < ideal; ++index)
	{
		pthread_join(workers[index], NULL);
	}

	// Sum all the worker's individual sums
	for(size_t index = 0; index < ideal; ++index)
	{
		sum += worker_data_array[index].my_sum;
	}	

	// After sum is made, multiply by this value
	sum *= delta/2.0;

	fprintf(stdout, "The area under the function is: %.2lf. Elapsed %.3lfs. %ld workers.\n", sum, walltime_elapsed(&start), ideal);

	return 0;
}
