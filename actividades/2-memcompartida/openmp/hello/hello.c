#include <stdio.h>
#include <omp.h>
#include <stdlib.h>

void hello(void)
{
	int worker_number = omp_get_thread_num();
	int total_workers = omp_get_num_threads();
	printf("heya from worker %d of %d\n", worker_number, total_workers-1);
}

int main(int argc, char* argv[])
{
	int n_threads = omp_get_max_threads();
	if (argc > 1)
		n_threads = atoi(argv[1]);
	
	#pragma omp parallel num_threads(n_threads)
	hello();
	
	printf("heya from main thread!\n"); 
	return 0;
}
