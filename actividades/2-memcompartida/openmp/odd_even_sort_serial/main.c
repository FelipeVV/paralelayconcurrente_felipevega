#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void swap(double *a, double *b)
{
	double t = *b;
	*b = *a;
	*a = t;
}

void serial_odd_even_sort(size_t n, double a[n])
{
	for ( size_t phase = 0; phase < n; ++phase )
	{
		if ( phase % 2 == 0 )
		{
			for ( size_t i = 1; i < n; i += 2 )
				if ( a[i - 1] > a[i] )
					swap( &a[i - 1], &a[i] );
		}
		else
		{
			for ( size_t i = 1; i < n - 1; i += 2 )
				if ( a[i] > a[i + 1] )
					swap( &a[i], &a[i + 1]);
		}
	}
}

void main(int argc, char* argv[])
{
	srand(time(NULL));
	int array_length = 0;
	if (argc > 1)
		array_length = atoi(argv[1]);
	double arr[array_length];

	for(int i = 0; i < array_length; ++i)
	{
		arr[i] = (double) rand() / (double) rand();
		fprintf(stderr, "[%.2f]", arr[i]);
	}
	fprintf(stderr, "\n");

	serial_odd_even_sort(array_length, arr);	

	for(int i = 0; i < array_length; ++i)
	{
		fprintf(stderr, "[%.2f]", arr[i]);
	}
	fprintf(stderr, "\n");
}