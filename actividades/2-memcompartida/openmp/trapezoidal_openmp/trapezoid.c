#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <omp.h>

#include "concurrency.h"

/// cool function
double f(double x)
{
	return x /*+ sin(2.0*x)*/;
}

/// https://wikimedia.org/api/rest_v1/media/math/render/svg/bfac656c5d75fb6dd578fd1a96cc5d23ef6a3692
double trapezoidal_rule(double a, double b, size_t n, int n_threads)
{
	double sum = 0.0;
	double delta = (b-a)/(double)n;
	sum += f(a + 0.0*delta) + f(a + (size_t)n*delta);
	#pragma omp parallel for num_threads(n_threads) default(none) shared(sum, a, delta, n)
	for(size_t i = 1; i < n; ++i)
	{
		#pragma omp critical
		sum += 2.0*f(a + i * delta);
	}
	
	sum *= delta/2.0;
	return sum;
}

int main(int argc, char* argv[])
{
	double a = 0.0;
	double b = 0.0;
	double n = 1.0;
	int n_threads = omp_get_max_threads();

	if( argc > 3 )
	{
		a = (double)atof(argv[1]);
		b = (double)atof(argv[2]);
		n = (double)atof(argv[3]);
//		fprintf(stderr, "%lf, %lf and %lf\n", a, b, n);
	}
	else
		return fprintf(stderr, "Not enough parameters.\n"), 420;

	if(argc > 4)
		n_threads = (int)atoi(argv[4]);

	// Start counting the time
	walltime_t start;
	walltime_start(&start);
	double ans = trapezoidal_rule(a, b, n, n_threads);
	fprintf(stdout, "The area under the function is: %.2lf. Elapsed %.3lfs.\n", ans, walltime_elapsed(&start));

	return 0;
}
