#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void swap(double *a, double *b)
{
	double t = *b;
	*b = *a;
	*a = t;
}

// http://jeisson.ecci.ucr.ac.cr/concurrente/2018b/ejemplos/source.php?file1=2-openmp/parallel_for/parallel_for.c
// http://jeisson.ecci.ucr.ac.cr/concurrente/2018b/ejemplos/source.php?file1=2-openmp/several_for/several_for.c
void serial_odd_even_sort(size_t n, double a[n], size_t w_len)
{
	for ( size_t phase = 0; phase < n; ++phase )
	{
		if ( phase % 2 == 0 )
		{
			#pragma omp parallel for num_threads(w_len) default(none) \
				shared(n, a)
			for ( size_t i = 1; i < n; i += 2 )
				if ( a[i - 1] > a[i] )
					swap( &a[i - 1], &a[i] );
		}
		else
		{
			#pragma omp parallel for num_threads(w_len) default(none) \
				shared(n, a)
			for ( size_t i = 1; i < n - 1; i += 2 )
				if ( a[i] > a[i + 1] )
					swap( &a[i], &a[i + 1]);
		}
	}
}

void main(int argc, char* argv[])
{
	srand(time(NULL));
	int array_length = 0;
	int w_len = omp_get_max_threads();
	if (argc > 1)
		array_length = atoi(argv[1]);
	if  (argc > 2)
		w_len = atoi(argv[2]);
	double arr[array_length];

	for(int i = 0; i < array_length; ++i)
	{
		arr[i] = (double) rand() / (double) rand();
		fprintf(stderr, "[%.2f]", arr[i]);
	}
	fprintf(stderr, "\n");

	serial_odd_even_sort(array_length, arr, w_len);	

	for(int i = 0; i < array_length; ++i)
	{
		fprintf(stderr, "[%.2f]", arr[i]);
	}
	fprintf(stderr, "\n");
}