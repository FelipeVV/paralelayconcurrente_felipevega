#include <iostream>
#include <QDir>
#include <QFile>
#include <QTextStream>

#include "goldbachcalculator.h"
#include "goldbachtester.h"

GoldbachTester::GoldbachTester(int &argc, char **argv)
    : QCoreApplication(argc, argv)
{
}

int GoldbachTester::run()
{
    std::cout << "GoldbachTester v1.5 modified by Felipe Vega" << std::endl << std::endl;
    if(this->arguments().count() < 2)
    {
        return std::cout << "Folder(s) not specified!" << std::endl << "Usage: GoldbachTester test_folder1 test_folder2 ... test_folderN   " << std::endl, 1;
    }
    for ( int index = 1; index < this->arguments().count(); ++index )
    {
        bool success = this->testDirectory( this->arguments()[index] );
        if(!success)
            return std::cerr << "error: empty folder." << std::endl, 2;
    }

    return this->exec();
}

bool GoldbachTester::testDirectory(const QString &path)
{
    std::cout << "Directory : " << qPrintable(path) << std::endl;

    QDir dir(path);
    if ( ! dir.exists() )
        return std::cerr << "error: could not open directory: " << qPrintable(path) << std::endl, false;

    dir.setFilter(QDir::Files);

    QFileInfoList fileList = dir.entryInfoList();
    if (fileList.count() == 0)
        return false;

    for ( int index = 0; index < fileList.count(); ++index )
        this->testFile( fileList[index] );

    return true;
}

bool GoldbachTester::testFile(const QFileInfo &fileInfo)
{
    std::cout << "Testing " << qPrintable( fileInfo.filePath() ) << std::endl;

    bool ok = true;
    this->number = fileInfo.baseName().toLongLong(&ok);
    if ( ! ok )
        return std::cerr << "error: invalid number: " << qPrintable(fileInfo.fileName()) << std::endl, false;

    GoldbachCalculator* goldbachCalculator = new GoldbachCalculator(this);
    this->calculators.insert(goldbachCalculator, fileInfo);
    this->connect( goldbachCalculator, &GoldbachCalculator::calculationDone, this, &GoldbachTester::calculationDone );
    goldbachCalculator->calculate(number);

    return true;
}

void GoldbachTester::calculationDone(long long sumCount)
{
    Q_UNUSED(sumCount);

    GoldbachCalculator* goldbachCalculator = dynamic_cast<GoldbachCalculator*>( sender() );
    Q_ASSERT(goldbachCalculator);

    long long currentNumber = goldbachCalculator->getNumber();
    const QFileInfo& fileInfo = this->calculators.value( goldbachCalculator );
    const QVector<QString>& expectedSums = this->loadLines(fileInfo);
    const QVector<QString>& calculatorSums = goldbachCalculator->getAllSums();


    ++testCases;
    bool aLineFailed = false;
    for(int line = 0; line < expectedSums.size(); ++line)
    {
        if(expectedSums[line] == "")
        {
            break; ///quick fix. even if its not good looking. TODO: make this more elegant and attractive to the eye.
        }
        if(expectedSums[line] != calculatorSums[line])
        {
            std::cout << "test case " << currentNumber << " failed at line " << (line+1) << ":" << std::endl;
            std::cout << "expected: " << expectedSums[line].toStdString() << std::endl;
            std::cout << "recieved: " << calculatorSums[line].toStdString() << std::endl;
            aLineFailed =  true;
        }
    }

    if(aLineFailed)
    {
        ++errors;
    }
    else
    {
        ++passed;
    }


    goldbachCalculator->deleteLater();
    this->calculators.remove( goldbachCalculator );
    if ( this->calculators.count() <= 0 )
    {
        this->quit();
        std::cout << std::endl << this->testCases << " test cases: "
                  << this->passed << " passed (" << ( ( passed/ static_cast<double>(testCases) ) * 100.0)
                  << "%), " << this->errors << " failed (" << ( (errors/ static_cast<double>(testCases) ) * 100.0 )
                  << "%)." << std::endl;
    }
}

QVector<QString> GoldbachTester::loadLines(const QFileInfo &fileInfo)
{
    QVector<QString> result;

    QFile file( fileInfo.absoluteFilePath() );
    if ( ! file.open(QFile::ReadOnly) )
        return std::cerr << "error: could not open: " << qPrintable(fileInfo.fileName()) << std::endl, result;

    QTextStream textStream( &file );
    while ( ! textStream.atEnd() )
        result.append( textStream.readLine() );

    return result;
}
