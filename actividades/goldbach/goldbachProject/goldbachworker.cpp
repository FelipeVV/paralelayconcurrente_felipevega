#include <bits/stdc++.h>
#include <QDebug>
#include <QtGlobal>
#include <QtMath>

#include "goldbachworker.h"

#define MIN(a,b) ((a) < (b) ? (a) : (b))
#define MAX(a,b) ((a) > (b) ? (a) : (b))

GoldbachWorker::GoldbachWorker(long long number,
                               int workerNumber,
                               int workerCount,
                               QVector<QString> &results,
                               int startingIndex,
                               int finishingIndex,
                               double totalNumberIterations,
                               double expectedPercentageSignals,
                               QVector<bool>& sieve,
                               QObject *parent)
    : QThread(parent)
    , parent{parent}
    , number{number}
    , workerNumber{workerNumber}
    , workerCount{workerCount}
    , results{results}
    , startingIndex{startingIndex}
    , finishingIndex{finishingIndex}
    , totalNumberIterations{totalNumberIterations}
    , sieve{sieve}
    , expectedPercentageSignals{expectedPercentageSignals}
{
    this->myNumberIterations = totalNumberIterations/static_cast<double>(workerCount);
    this->iterationsUntilPercentage = myNumberIterations/expectedPercentageSignals;

    /// Create primeList's first number's primality
    /// 0 and 1 is non prime (write 0)
    /// 2 and 3 are prime (write 1)
    /*primeList.append(0); //0
    primeList.append(0); //1
    primeList.append(1); //2
    primeList.append(1); //3
    primeList.append(0); *///4
}

QVector<QString> &GoldbachWorker::getResults()
{
    return  this->results;
}

void GoldbachWorker::run()
{
    time.start();
    this->calculate();
    double seconds = time.elapsed() / 1000.0;
//    qDebug() << "worker #" << workerNumber << "lasts " << seconds << "seconds and found " << results.size() << "sums.";
    emit this->calculationDone(this->workerNumber);
}

int GoldbachWorker::calculate()
{
    if ( this->number < 4 || this->number == 5 ) return 0;
    return this->number % 2 == 0 ? calculateEvenGoldbach() : calculateOddGoldbach();
}

void GoldbachWorker::incrementAcumulatedLoopsAndCheckPercentage()
{
    ++this->acumulatedLoops;
    if(acumulatedLoops >= iterationsUntilPercentage)
    {
        reportProgress();
        this->acumulatedLoops = this->acumulatedLoops - this->iterationsUntilPercentage;
    }
}

int GoldbachWorker::calculateEvenGoldbach()
{
    for ( long long a = startingIndex; a < finishingIndex; ++a )
    {
        incrementAcumulatedLoopsAndCheckPercentage();
        if ( ! isPrime(a) ) continue;
        long long b = number - a;
        if ( b >= a && isPrime(b) )
            this->results.append( tr("%1 + %2").arg(a).arg(b) );

        // If user cancelled, stop calculations
        if ( this->isInterruptionRequested() )
            return 1;
    }
    return 0;
}

int GoldbachWorker::calculateOddGoldbach()
{
    for ( long long a = startingIndex; a < finishingIndex; ++a )
    {
        incrementAcumulatedLoopsAndCheckPercentage();
        if ( ! isPrime(a) ) continue;
        for ( long long b = a; b < number; ++b )
        {
            if ( ! isPrime(b) ) continue;
            long long c = number - a - b;
            if ( c >= b && isPrime(c) )
                this->results.append( tr("%1 + %2 + %3").arg(a).arg(b).arg(c) );

            if ( this->isInterruptionRequested() )
                return 1;
        }
    }
    return 0;
}



bool GoldbachWorker::addToPrimeList(long long number, int isPrime)
{
    if(this->primeList.size() == number) // If number is next on the list
    {
        primeList.append(isPrime);
        return true;
    }

    /// pairing unsuccesful
    return false;
}

bool GoldbachWorker::isPrime(long long number)
{
    //return isPrimeMyAttempt(number);
    //return isPrimeOld(number);
    return this->sieve[number];
}

bool GoldbachWorker::isPrimeMyAttempt(long long number)
{
    if ( number < 2 ) return false;
    if (number == 2) return true;
    if (number % 2 == 0) return false;

    if( (primeList.size()-1) >= number) // If number is in the prime dictionary
    {
        if( primeList[number] == 1)
            return true;
        return false;
    }

    for ( long long i = 3, last = static_cast<long long>(qSqrt(number)); i <= last; i+=2 )
    {
        if ( number % i == 0 )
        {
            addToPrimeList(number, 0);
            return false;
        }
    }

    addToPrimeList(number, 1);
    return true;
}

bool GoldbachWorker::isPrimeOld(long long number)
{
    if ( number < 2 ) return false;

    for ( long long i = 2, last = qSqrt(number); i <= last; ++i )
    {
        if ( number % i == 0 )
        {
            return false;
        }
    }

    return true;
}

/*

/// C++ program Miller-Rabin primality test
// Utility function to do modular exponentiation.
// It returns (x^y) % p
int power(int x, unsigned int y, int p)
{
    int res = 1;      // Initialize result
    x = x % p;  // Update x if it is more than or
                // equal to p
    while (y > 0)

    {
        // If y is odd, multiply x with result
        if (y & 1)
            res = (res*x) % p;

        // y must be even now
        y = y>>1; // y = y/2
        x = (x*x) % p;
    }
    return res;
}

// This function is called for all k trials. It returns
// false if n is composite and returns false if n is
// probably prime.
// d is an odd number such that  d*2<sup>r</sup> = n-1
// for some r >= 1
bool miillerTest(int d, int n)
{
    // Pick a random number in [2..n-2]
    // Corner cases make sure that n > 4
    int a = 2 + std::rand() % (n - 4);

    // Compute a^d % n
    int x = power(a, d, n);

    if (x == 1  || x == n-1)
       return true;

    // Keep squaring x while one of the following doesn't
    // happen
    // (i)   d does not reach n-1
    // (ii)  (x^2) % n is not 1
    // (iii) (x^2) % n is not n-1
    while (d != n-1)
    {
        x = (x * x) % n;
        d *= 2;

        if (x == 1)      return false;
        if (x == n-1)    return true;
    }

    // Return composite
    return false;
}

// It returns false if n is composite and returns true if n
// is probably prime.  k is an input parameter that determines
// accuracy level. Higher value of k indicates more accuracy.
bool miiller_isPrime(int n, int k = 200)
{
    // Corner cases
    if (n <= 1 || n == 4)  return false;
    if (n <= 3) return true;

    // Find r such that n = 2^d * r + 1 for some r >= 1
    int d = n - 1;
    while (d % 2 == 0)
        d /= 2;

    // Iterate given nber of 'k' times
    for (int i = 0; i < k; i++)
         if (miillerTest(d, n) == false)
              return false;

    return true;
}

*/
