#ifndef GOLDBACHWORKER_H
#define GOLDBACHWORKER_H

#include <QThread>
#include <QTime>
#include <QVector>

#include "goldbachcalculator.h"

class GoldbachWorker : public QThread
{
    Q_OBJECT

  protected:
    GoldbachCalculator parent;
    long long number = 0;
    int workerNumber = 0;
    int workerCount = 0;
    QVector<QString>& results;
    int startingIndex = 0;
    int finishingIndex = 0;
    /// Progress bar related
    double acumulatedLoops = 0;
    double totalNumberIterations = 0;
    double expectedPercentageSignals = 0;
    double myNumberIterations = 0;
    double iterationsUntilPercentage = 0;

    /// Sieve
    QVector<bool>& sieve;

    /// Prime list is a "dictionary" of sorts for prime numbers. It's a quick reference to whether a number is prime or not.
    /// Prime list is a list of numbers. Each index will store either a 1 or a 0.
    /// if 0, the index the number is stored is not prime
    /// if 1, the index the number is stored is prime
    /// Examples:
    /// primeList[4] == 0
    /// primeList[5] == 1
    /// primeList[9] == 0
    QVector<int> primeList;
    QTime time;
    ////METHODS
    bool addToPrimeList(long long number, int isPrimeOld);
    /// To quickly switch isPrime methods
    bool isPrime(long long number);
    /// Optimized isPrime
    bool isPrimeMyAttempt(long long number);
    /// Internet solution
    bool miiller_isPrime(int n, int k = 200);
    bool miillerTest(int d, int n);
    int power(int x, unsigned int y, int p);
    void incrementAcumulatedLoopsAndCheckPercentage();

  public:
    explicit GoldbachWorker(long long number,
                            int workerNumber,
                            int workerCount,
                            QVector<QString>& results,
                            int startingIndex,
                            int finishingIndex, double totalNumberIterations, double expectedPercentageSignals,
                            QVector<bool>& sieve,
                            QObject *parent = nullptr);
    QVector<QString>& getResults();

  protected:
    void run() override;

  signals:
    void calculationDone(int workerIndex);
    void reportProgress();

  public slots:

  protected:
    /**
    * @brief Calcula las sumas de Goldbach para el numero dado y las agrega a una pizarra
    * @param number El numero dado por el usuario
    * @return La cantidad de sumas encontradas
    */
    int calculate();
    /**
    * Calcula todas las sumas de dos primos que equivalen al numero dado, y las presenta
    * en el area resultado. Incrementa la barra de progreso mientras realiza el calculo
    * de la conjetura fuerte de Goldbach, por ejemplo:
    *
    *   4 == 2 + 2
    *   6 == 3 + 3
    *  10 == 3 + 7 == 5 + 5
    *
    * @param numero Un numero entero par mayor o igual a 4
    * @return la cantidad de sumas de dos primos encontradas
    */
    int calculateEvenGoldbach();
    /**
    * Calcula todas las sumas de tres primos que equivalen al numero dado, y las presenta
    * en el area resultado. Incrementa la barra de progreso mientras realiza el calculo
    * de la conjetura debil de Goldbach, por ejemplo:
    *
    *   7 == 2 + 2 + 3
    *   9 == 2 + 2 + 5 == 3 + 3 + 3
    *
    * @param numero Un numero entero impar mayor o igual a 7
    * @return la cantidad de sumas de tres primos encontradas
    */
    int calculateOddGoldbach();
    /**
    * Retorna true si numero es primo, false si numero no es primo o menor que 2
    * Por definicion 1 no es primo ni compuesto, este metodo retorna false
    */
    bool isPrimeOld(long long number);

};

#endif // GOLDBACHWORKER_H
