#ifndef GOLDBACHCALCULATOR_H
#define GOLDBACHCALCULATOR_H

#include <QAbstractListModel>
#include <QVector>

class GoldbachWorker;

class GoldbachCalculator : public QAbstractListModel
{
    Q_OBJECT
    Q_DISABLE_COPY(GoldbachCalculator)

  protected:
    /// ???
    int fetchedRowCount = 0;
    int finishedWorkers = 0;
    int ideal = 0;
    long long number = 0;
    double totalPercentageSignals = 0;
    QVector<bool> sieve;
    QVector<QVector<QString>> results;
    QVector<GoldbachWorker*> workers;

    /// Percentage related
    int expectedPercentageSignalsFromEachWorker = 20;
    long long totalNumberIterations = 0;

    /// Methods
    long long calculateStartingIndex(long long number, int workerNumber, int workerCount);
    long long calculateFinishingIndex(long long number, int workerNumber, int workerCount);
    int getTotalSumCount() const;
    QString getResultOnIndex(int position) const;
    long long calculateTotalNumberIterations();
    void fillSieve();

  public:
    explicit GoldbachCalculator(QObject *parent = nullptr);
    void calculate(long long number);
    void stop();
    QVector<QString> getAllSums() const;
    long long getNumber();
    bool consultSieve(int n) const;

  public: // Overriden methods from QAbastractListModel
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

  protected:
    virtual bool canFetchMore(const QModelIndex &parent) const override;
    virtual void fetchMore(const QModelIndex &parent) override;

  signals:
    void calculationDone(long long sumCount);
    void requestProgressBarUpdate(int percent);

  protected slots:
    void workerDone(int workerIndex);
    void updatePercentage();
};

#endif // GOLDBACHCALCULATOR_H
