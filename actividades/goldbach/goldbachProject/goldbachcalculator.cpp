#include <QDebug>
#include <QTimer>
#include "goldbachcalculator.h"
#include "goldbachworker.h"

/**
 * @brief Constructor, on big soup rice.
 * @param parent Obligatory
 */
GoldbachCalculator::GoldbachCalculator(QObject *parent)
    : QAbstractListModel(parent)
{
}

/**
 * @brief GoldbachCalculator::calculate
 * @param number asked by View class (mainWindow)
 */
void GoldbachCalculator::calculate(long long number)
{
    this->beginResetModel();
    this->number = number;
    this->ideal = QThread::idealThreadCount() - 1;

    /// sieve stuff
    if(number>0)
    {
        this->sieve.resize(number+1);
        this->sieve.fill(true, number+1);
#ifdef GOLDBACH
        qDebug() << "size: " << sieve.size();
#endif
        fillSieve();
    }

    /// Percentage stuff.
    /// Calculate the total number of iterations based on the given number
    this->totalNumberIterations = calculateTotalNumberIterations();

#ifdef GOLDBACH
    qDebug() << "cores: " << this->ideal;
#endif

    /// Create sub-vectors
    for(int index = 0; index < this->ideal; ++index)
    {
        this->results.append(QVector<QString>());
    }

    /// Create the workers
    for ( int current = 0; current < this->ideal; ++current  )
    {
        long long startingIndex = calculateStartingIndex(number, current, this->ideal);
        long long finishingIndex = calculateFinishingIndex(number, current, this->ideal);
        GoldbachWorker* goldbachWorker = new GoldbachWorker(number,
                                                            current,
                                                            this->ideal,
                                                            this->results[current],
                                                            startingIndex,
                                                            finishingIndex,
                                                            this->totalNumberIterations,
                                                            this->expectedPercentageSignalsFromEachWorker,
                                                            this->sieve,
                                                            this);
        this->connect( goldbachWorker,
                       &GoldbachWorker::calculationDone,
                       this,
                       &GoldbachCalculator::workerDone );
        this->connect( goldbachWorker,
                       &GoldbachWorker::reportProgress,
                       this,
                       &GoldbachCalculator::updatePercentage );
        this->workers.append(goldbachWorker);
        goldbachWorker->start();
    }


    this->fetchedRowCount = 0;
    this->endResetModel();
}

long long GoldbachCalculator::calculateStartingIndex(long long number, int workerNumber, int workerCount)
{
    long long half = (number / 2) + 1;
    /// First condition
    if(workerNumber == 0)
    {
        return 2;
    }

    long long slice = half / workerCount;
    return slice * workerNumber;
}


long long GoldbachCalculator::calculateFinishingIndex(long long number, int workerNumber, int workerCount)
{
    long long half = (number / 2) + 1;
    /// Last condition
    if(workerNumber == workerCount-1)
    {
        return half;
    }

    long long slice = half / workerCount;
    return slice * (workerNumber+1);
}

int GoldbachCalculator::getTotalSumCount() const
{
    int tempSums = 0;
    for(int vector = 0; vector < results.size(); ++vector)
    {
        tempSums += results[vector].size();
    }
    return tempSums;
}

void GoldbachCalculator::stop()
{
    for ( int index = 0; index < this->ideal; ++index )
    {
        GoldbachWorker* goldbachWorker = this->workers[index];
        if( goldbachWorker )
            goldbachWorker->requestInterruption();
    }
}

QVector<QString> GoldbachCalculator::getAllSums() const
{
    int count = 0;
    QVector<QString> tempVector;
    for(int vector = 0; vector < results.size(); ++vector)
    {
        for(int index = 0; index < results[vector].size(); ++index)
        {
            tempVector.append( tr("%1: ").arg(count+1) + results[vector][index]);
            ++count;
        }
    }
    return tempVector;
}

long long GoldbachCalculator::getNumber()
{
    return this->number;
}

/// Heredado: ???
int GoldbachCalculator::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return this->fetchedRowCount;
}


QVariant GoldbachCalculator::data(const QModelIndex &index, int role) const
{
    if ( ! index.isValid() )
        return QVariant();

    if ( index.row() < 0 || index.row() >= getTotalSumCount() )
        return QVariant();

    if (role == Qt::DisplayRole)
    {
        int vectorIndex = index.row();
        return getResultOnIndex(vectorIndex);
    }

    return QVariant();
}

/**
 * @brief GoldbachCalculator::getResultOnIndex: Algorithm that maps together all the vectors inside resultsVector.
 * We can imagine the result of the mapping of the subvectors as a "big vector".
 * @param position: Index of the "big vector" that has been requested.
 * @return: The value of the "big vector"s index.
 */
QString GoldbachCalculator::getResultOnIndex(int position) const
{
    int index = 0;
    int currentSubVector = 0;
    while( ((index + results[currentSubVector].size()) <= position)&&(currentSubVector < this->results.size()) )
    {
        index += results[currentSubVector].size();
        ++currentSubVector;
    }

    if( (currentSubVector < this->results.size())&&((position-index) < results[currentSubVector].size()) )
    {
        QString str = tr("%1: ").arg(position + 1) + results[currentSubVector][position - index];
        return str;
    }
    return "ERROR!";
}

long long GoldbachCalculator::calculateTotalNumberIterations()
{
    return ( (this->number+1) / 2);
}

bool GoldbachCalculator::canFetchMore(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return this->fetchedRowCount < getTotalSumCount();
}

void GoldbachCalculator::fetchMore(const QModelIndex &parent)
{
	Q_UNUSED(parent);
    int remainder = getTotalSumCount() - this->fetchedRowCount;
    int itemsToFetch = qMin(100, remainder);

    if (itemsToFetch <= 0) ///really just needs to be ==
        return;

    int firstRow = this->fetchedRowCount;
    int lastRow = this->fetchedRowCount + itemsToFetch - 1; // Por que el -1 ???
    beginInsertRows(QModelIndex(), firstRow, lastRow); //Por que el fetched RowCount
                                                       //(que ya esta impreso) debe volver a imprimirse ???
    this->fetchedRowCount += itemsToFetch;
    endInsertRows();
}

void GoldbachCalculator::workerDone(int workerIndex)
{
    this->finishedWorkers++;
    QTimer::singleShot( 5, this->workers[workerIndex], SLOT(deleteLater()) );
    workers[workerIndex] = nullptr;

    /// If all workers are finished...
    if(this->finishedWorkers >= (this->ideal))
    {
        emit this->calculationDone(this->getTotalSumCount());
    }
}

void GoldbachCalculator::updatePercentage()
{
    ++totalPercentageSignals;
    double percentage = (this->totalPercentageSignals / (this->expectedPercentageSignalsFromEachWorker * this->ideal)) * 100.0;
#ifdef GOLDBACH
    qDebug() << "-------------- got: " << totalPercentageSignals
             << "Expect: "
             << this->expectedPercentageSignalsFromEachWorker
             << "*" << this->ideal << "="
             << this->expectedPercentageSignalsFromEachWorker * this->ideal
             << "(" << percentage << "%)";
#endif
    requestProgressBarUpdate(percentage);
}

void GoldbachCalculator::fillSieve()
{
    // Create a boolean array "prime[0..n]" and initialize
    // all entries it as true. A value in prime[i] will
    // finally be false if i is Not a prime, else true.
    for (int p=2; p*p <= this->number; p++)
    {
        // If prime[p] is not changed, then it is a prime
        if (sieve[p] == true)
        {
            // Update all multiples of p
            for (int i=p*2; i<=this->number; i += p)
                sieve[i] = false;
        }
    }
}

