#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "levenshtein.h"

#define ALPHABET_SIZE 256

/**
 * @brief levenshtein. Algorithm based on https://ieeexplore.ieee.org/document/6665373
 * @param s1
 * @param s2
 * @return distance between the strings based on lehvenshtein algorithm.
 */
int levenshtein(char *s1, char *s2, int workers)
{
    int s1len = strlen(s1);
    int s2len = strlen(s2);

    if(s1len > s2len)
        return parallel_levdist(s1, s2, s1len, s2len, workers);
    return parallel_levdist(s2, s1, s2len, s1len, workers);
}

/**
 * @brief create_matrix. from jeisson.ecci.ucr.ac.cr
 * @param rows
 * @param cols
 * @return a dynamically allocated matrix
 */
int** create_matrix(int rows, int cols)
{
   // Allocate a vector of vectors
   int** matrix = (int**)calloc(rows, sizeof(int*));
   for (int row_index = 0; row_index < rows; ++row_index )
      matrix[row_index] = (int*)calloc(cols, sizeof(int));
    return matrix;
}

/**
 * @brief free_matrix. from jeisson.ecci.ucr.ac.cr.
 * @param matrix
 * @param rows
 * @param cols
 */
void free_matrix(int** matrix, int rows)
{
    // Free each row first, then the vector of vectors
    for (int row_index = 0; row_index < rows; ++row_index )
        free(matrix[row_index]);
    free(matrix);
}

void debug_print_matrix(int** matrix, int rows, int cols)
{
    for(int row = 0; row < rows; row++)
    {
        for(int col = 0; col < cols; col++)
        {
            fprintf(stderr, "[%d]", matrix[row][col]);
        }
        fprintf(stderr, "\n");
    }
    fprintf(stderr, "---------------------\n");
}

typedef struct
{
   size_t my_id;
   size_t my_total_workers;

   size_t my_rows;
   size_t my_t_len;
   int** X;
   char* text;
   char* alphabet;

   size_t my_p_len;
   char* pattern;
   int** D;
   pthread_barrier_t* shared_barrier;
} worker_data_t;

void* worker_fill_X(void* data)
{
    worker_data_t *my_data = (worker_data_t*)data;
    size_t dist = my_data->my_rows / my_data->my_total_workers;
    size_t start = my_data->my_id * dist;
    size_t end = 0;
    if(my_data->my_id == my_data->my_total_workers - 1)
        end = my_data->my_rows;
    else
        end = (my_data->my_id+1) * dist;
    size_t t_len = my_data->my_t_len;
    int** X = my_data->X;
    char* text = my_data->text;
    char* alphabet = my_data->alphabet;
    //fprintf(stderr, "[%d-%d]", start, end);
    for(size_t i = start; i < end; ++i) //traverse las rows que me tocan
    {
        for(size_t j = 0; j <= t_len; ++j) //cols
        {
            //fprintf(stderr, "X[%d(muere %d)][%d(muere %d)]?\n", i, ALPHABET_SIZE, j, t_len+1);
            if(j == 0)
            {
                //fprintf(stderr, "1");
                X[i][j] = 0;
            }
            else if(text[j - 1] == alphabet[i])
            {
                //fprintf(stderr, "2");
                X[i][j] = j;
            }
            else
            {
                //fprintf(stderr, "3");
                X[i][j] = X[i][j - 1];
            }
        }
    }
    return NULL;
}

void* worker_fill_D(void* data)
{
    worker_data_t *my_data = (worker_data_t*)data;
    size_t t_len = my_data->my_t_len;
    size_t dist = t_len / my_data->my_total_workers;
    size_t start = my_data->my_id * dist;
    size_t end = 0;
    if(my_data->my_id == my_data->my_total_workers - 1)
        end = t_len;
    else
        end = (my_data->my_id+1) * dist;
    size_t p_len = my_data->my_p_len;
    int** X = my_data->X;
    int** D = my_data->D;
    char* text = my_data->text;
    char* alphabet = my_data->alphabet;
    char* pattern = my_data->pattern;
    pthread_barrier_t* barrier = my_data->shared_barrier;
    for(int i = 0; i <= (int)p_len; ++i)
    {
        for(int j = (int)start; j <= (int)end; ++j)
        {
            /// Index for referring to the cell that is upward to us
            int north_index = (i - 1)%2;
            /// Set the value that will be assigned to D[i][j]
            int value = 0;
            /// Get the value of c
            int c = get_c(pattern[i - 1], alphabet);
            if(i == 0)
            {
                value = j;
            }
            else if(j == 0)
            {
                value = i;
            }
            else if(text[j - 1] == pattern[i - 1])
            {
                value = D[north_index][j - 1];
            }
            else if(X[c][j] == 0)
            {
                value = 1 + MIN3(D[north_index][j], D[north_index][j - 1], j + i - 1);
            }
            else
            {
                int x = D[north_index][j];
                int y = D[north_index][j - 1];
                int z = D   [north_index]
                            [X[c][j] - 1]
                            +(j - 1 - X[c][j]);
                value = 1 + MIN3( x , y , z );
            }
            D[i%2][j]=value;
        }
        pthread_barrier_wait(barrier);
    }
    return NULL;
}

/**
 * @brief parallel_levdist
 * @param text string
 * @param pattern string
 * @param t_len length
 * @param p_len length
 * @return distance between the two strings
 */
int parallel_levdist(char* text, char* pattern,int t_len, int p_len, int ideal_recommended)
{
    /// Create matrixes and alphabet string
    int** D = create_matrix(2 ,t_len + 1);
    int** X = create_matrix(ALPHABET_SIZE, t_len + 1);
    char alphabet[ALPHABET_SIZE];

    /// Fill the alphabet array with their respective chars
    for(int i = 0; i < ALPHABET_SIZE; ++i)
    {
        alphabet[i] = i;
    }

    size_t ideal = ideal_recommended;
    // Create worker array
    pthread_t workers[ideal];
    // Create data array
    worker_data_t worker_data_array[ideal];

    /// Fill X[][]
    /// Put workers to work
    for(size_t index = 0; index < ideal; ++index)
    {
        worker_data_array[index].my_id = index;
        worker_data_array[index].my_total_workers = ideal;
        worker_data_array[index].my_rows = ALPHABET_SIZE;
        worker_data_array[index].my_t_len = t_len;
        worker_data_array[index].X = X;
        worker_data_array[index].text = text;
        worker_data_array[index].alphabet = alphabet;
        pthread_create( &workers[index],
                        NULL,
                        worker_fill_X,
                        (void*)&worker_data_array[index]
                        );
    }
    // wait
    for(size_t index = 0; index < ideal; ++index)
    {
        pthread_join(workers[index], NULL);
    }

    /// Fill D[][]
    pthread_barrier_t barrier;
    pthread_barrier_init(&barrier, NULL, (unsigned)ideal);
    /// Put workers to work
    for(size_t index = 0; index < ideal; ++index)
    {
        worker_data_array[index].my_id = index;
        worker_data_array[index].my_total_workers = ideal;
        worker_data_array[index].my_t_len = t_len;
        worker_data_array[index].my_p_len = p_len;
        worker_data_array[index].X = X;
        worker_data_array[index].D = D;
        worker_data_array[index].text = text;
        worker_data_array[index].pattern = pattern;
        worker_data_array[index].alphabet = alphabet;
        worker_data_array[index].shared_barrier = &barrier;
        pthread_create( &workers[index],
                        NULL,
                        worker_fill_D,
                        (void*)&worker_data_array[index]
                        );
    }
    // wait
    for(size_t index = 0; index < ideal; ++index)
    {
        pthread_join(workers[index], NULL);
    }
    pthread_barrier_destroy(&barrier);

    int distance = D[p_len%2][t_len];

    free_matrix(D,2);
    free_matrix(X,ALPHABET_SIZE);
    return distance;
}

int get_c(char searching, char* alphabet)
{
    int c = 0;
    for(int index = 0; index < ALPHABET_SIZE; ++index)
    {

        if(alphabet[index] == searching)
        {
            c = index;
        }
    }
    return c;
}

int MIN3(int a, int b, int c)
{
        return ((a) < (b) ? ((a) < (c) ? (a) : (c)) : ((b) < (c) ? (b) : (c)));
}

