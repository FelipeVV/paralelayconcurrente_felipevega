#ifndef LEVENSHTEIN_H
#define LEVENSHTEIN_H

/* This code is taken from wikibooks(dot)org
 * `levenshtein.h` - levenshtein
 * CC(Creative Commons) licensed.
 * Text is available under the Creative Commons Attribution-ShareAlike License.; additional terms may apply.
 *
 * Returns an unsigned integer, depicting
 * the difference between `a` and `b`.
 * See http://en.wikipedia.org/wiki/Levenshtein_distance
 * for more information.
 */
int levenshtein(char *s1, char *s2);

#endif // LEVENSHTEIN_H
