# output cuando no funciona:

compute-0-0.local:0: range [0, 0[ size 0
compute-0-0.local:1: range [0, 0[ size 0
compute-0-0.local:0:0: range [0,0[ size 0
compute-0-0.local:0:1: range [0,0[ size 0
compute-0-0.local:1:0: range [0,0[ size 0
compute-0-0.local:1:3: range [0,0[ size 0
compute-0-0.local:0:2: range [0,0[ size 0
compute-0-0.local:1:1: range [0,0[ size 0
compute-0-0.local:1:7: range [0,0[ size 0
compute-0-0.local:1:6: range [0,0[ size 0
compute-0-0.local:1:5: range [0,0[ size 0
compute-0-0.local:0:7: range [0,0[ size 0
compute-0-0.local:0:4: range [0,0[ size 0
compute-0-0.local:1:2: range [0,0[ size 0
compute-0-0.local:0:6: range [0,0[ size 0
compute-0-0.local:1:4: range [0,0[ size 0
compute-0-0.local:0:5: range [0,0[ size 0
compute-0-0.local:0:3: range [0,0[ size 0

# output cuando funciona: 

Enter the start: 3
Enter the end: 20
compute-0-0.local:0: range [3, 12[ size 9
compute-0-0.local:1: range [12, 20[ size 8
compute-0-0.local:0:0: range [3,5[ size 2
compute-0-0.local:0:6: range [10,11[ size 1
compute-0-0.local:0:1: range [5,6[ size 1
compute-0-0.local:0:3: range [7,8[ size 1
compute-0-0.local:0:4: range [8,9[ size 1
compute-0-0.local:0:2: range [6,7[ size 1
compute-0-0.local:0:5: range [9,10[ size 1
compute-0-0.local:0:7: range [11,12[ size 1
compute-0-0.local:1:0: range [12,13[ size 1
compute-0-0.local:1:2: range [14,15[ size 1
compute-0-0.local:1:1: range [13,14[ size 1
compute-0-0.local:1:5: range [17,18[ size 1
compute-0-0.local:1:6: range [18,19[ size 1
compute-0-0.local:1:4: range [16,17[ size 1
compute-0-0.local:1:3: range [15,16[ size 1
compute-0-0.local:1:7: range [19,20[ size 1

