#include <mpi.h>
#include <stdio.h>
#include <omp.h>
#include <stdlib.h>

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))
#define ABS(n) (((n)>(0))?(n):(-1*n))

void hello(int my_rank, char* hostname, int a, int b)
{
	int worker_number = omp_get_thread_num();
	int total_workers = omp_get_num_threads();
	
	int c = (b-a)/total_workers;
	int r = (b-a)%total_workers;

	int i = a + (worker_number*c) + MIN(worker_number, r);
	int f = a + ((worker_number+1)*c) + MIN(worker_number+1, r);

	printf("%s:%d:%d: range [%d,%d[ size %d\n", hostname, my_rank, worker_number, i, f, ABS(f - i) );
}

int main(int argc, char* argv[])
{

	
	MPI_Init(&argc, &argv);
	
	int a, b;
	if(argc >= 3)
	{
		// begining of range
		a = atoi(argv[1]);
		// ending of range
		b = atoi(argv[2]);
	}

	int my_rank = -1;
	int process_count = -1;

	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &process_count);

	char hostname[MPI_MAX_PROCESSOR_NAME];
	int len_hostname = -1;
	MPI_Get_processor_name(hostname, &len_hostname);

	// cociente
	int c = (b-a)/process_count;
	// residuo
	int r = (b-a)%process_count;
	
	int i = a + (my_rank * c) + MIN(my_rank, r);
	int f = a + ((my_rank+1) * c) + MIN(my_rank+1,r);

	printf("%s:%d: range [%d, %d[ size %d\n", hostname, my_rank, i, f, ABS(f-i) );
	
	int n_threads = omp_get_max_threads();
	#pragma omp parallel num_threads(n_threads) default(none) shared(my_rank, hostname, i, f)
	hello(my_rank, hostname, i, f);
	

	MPI_Finalize();
	return 0;
}
