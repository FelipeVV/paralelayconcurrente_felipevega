[B78244@arenal prime_hybrid_int]$ mpiexec -n 8 ./hello 2 500000
41538 primes found in range [2, 500000[ in 3.244884 with 8 processes and 32 threads

[B78244@arenal prime_process]$ mpiexec -n 8 ./hello 2 500000
41538 primes found in range [2, 500000[ in 4.807091 with 8 processes

[B78244@arenal prime_process_reduction]$ mpiexec -n 8 ./hello 2 500000
41538 primes found in range [2, 500000[ in 4.875215 with 8 processes
