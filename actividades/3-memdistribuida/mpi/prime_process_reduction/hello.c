#include <mpi.h>
#include <stdio.h>
#include <omp.h>
#include <stdlib.h>

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))
#define ABS(n) (((n)>(0))?(n):(-1*n))

int is_prime(int num)
{
	if (num <= 1) return 0;
     	if(num == 2) return 1;
	if (num % 2 == 0 && num > 2) return 0;
     	for(int i = 3; i < num / 2; i+= 2)
     	{
         	if (num % i == 0)
             		return 0;
     	}
	return 1;
}

int hello(int a, int b, int n_threads)
{
	(void)n_threads;
	int primes = 0;
	//#pragma omp parallel for num_threads(n_threads) default(none) shared(a, b) reduction(+:primes)
	for(int n = a; n < b; n++)
	{
		if(is_prime(n))
		{
			primes += 1;
		}
	}

	return primes;
}

int main(int argc, char* argv[])
{

	
	MPI_Init(&argc, &argv);
	
	

	int my_rank = -1;
	int process_count = -1;

	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &process_count);

	char hostname[MPI_MAX_PROCESSOR_NAME];
	int len_hostname = -1;
	MPI_Get_processor_name(hostname, &len_hostname);

	
	int a = 0;
	int b = 0;
	if(argc >= 2)
	{
		// begining of range
		a = atoi(argv[1]);
		// ending of range
		b = atoi(argv[2]);
	}
	else
	{
		if(my_rank == 0)
		{		
			printf("Enter the start: ");
			scanf("%d", &a);
			printf("Enter the end: ");
			scanf("%d", &b);
		}
	}
	MPI_Bcast(&a, 1, MPI_INT, 0, MPI_COMM_WORLD); 
	MPI_Bcast(&b, 1, MPI_INT, 0, MPI_COMM_WORLD); 
    

	// cociente
	int c = (b-a)/process_count;
	// residuo
	int r = (b-a)%process_count;
	
	int i = a + (my_rank * c) + MIN(my_rank, r);
	int f = a + ((my_rank+1) * c) + MIN(my_rank+1,r);
	
	int n_threads = omp_get_max_threads();
	int primes_counted = 0;
	double start_time, end_time;
	start_time = MPI_Wtime();
	primes_counted = hello(i, f, n_threads);
 
 	int total_primes = primes_counted; 
 	MPI_Reduce(&primes_counted, &total_primes, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD); 
	
	if(my_rank == 0)
	{
		end_time = MPI_Wtime();
		printf("%d primes found in range [%d, %d[ in %f with %d processes\n", total_primes, a, b, end_time-start_time, process_count);
	
	} 

	
//	printf("Process %d on %s found %d primes in range [%d, %d[ in %f with %d threads\n", my_rank, hostname, primes_counted, i, f, end_time - start_time, n_threads);
	MPI_Finalize();
	return 0;
}
