#include <mpi.h>
#include <stdio.h>
#include <omp.h>

void hello(int my_rank, char* hostname)
{
	int worker_number = omp_get_thread_num();
	int total_workers = omp_get_num_threads();
	printf("heya from worker %d of %d, of %d on %s\n", worker_number, total_workers-1, my_rank, hostname);
}

int main(int argc, char* argv[])
{
	MPI_Init(&argc, &argv);

	int my_rank = -1;
	int process_count = -1;

	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &process_count);

	char hostname[MPI_MAX_PROCESSOR_NAME];
	int len_hostname = -1;
	MPI_Get_processor_name(hostname, &len_hostname);

	printf("Hello from process %d of %d on %s\n", my_rank, process_count, hostname);
	
	int n_threads = omp_get_max_threads();
	#pragma omp parallel num_threads(n_threads) default(none) shared(my_rank, hostname)
	hello(my_rank, hostname);
	

	MPI_Finalize();
	return 0;
}
